#!/usr/bin/env python

# this node is the master control for creating demonstration videos of the robot performing guidance instructions for evacuees
# the user selects a preset demonstration point, then an instruction for the evacuee, then the platform type that should make the instruction

import roslib
import rospy
import time

from geometry_msgs.msg import Twist
from std_msgs.msg import String

tbGestureState = ""
tbMovementState = ""

distances = ["","near","far","stay"]
directions = ["","left","forward","backward","wait"]

def tbGestureCB(msg):
  global tbGestureState
  tbGestureState = msg.data

def tbMovementCB(msg):
  global tbMovementState
  tbMovementState = msg.data

def getDistOption():
  a = input("Select distance:\n1. Near\n2. Far\n3. Stay in place\n> ")
  return a

def getDirection():
  a = input("Select direction:\n1. Left\n2. Forward\n3. Backward\n4. Wait\n> ")
  return a

def getPlatform():
  a = input("Select platform\n1. Baseline\n2. Tablet\n3. Arms\n> ")
  return a

if __name__ == "__main__":
  rospy.init_node("turtlebot_master")
  rospy.Subscriber("/turtlebot_gesture_response", String, tbGestureCB)
  tbGestureCmd = rospy.Publisher("/turtlebot_gesture_command", String)
  tbMovementCmd = rospy.Publisher("/turtlebot_movement_command", String)
  rospy.Subscriber("/turtlebot_movement_response", String, tbMovementCB)
  tbArmCmd = rospy.Publisher("/arm_commands", String)
  
  dist = ""
  direction = ""
  platform = ""
  
  mode = 0
  while not rospy.is_shutdown():
    if mode == 0:
        dist = getDistOption()
        direction = getDirection()
        platform = getPlatform()
        
        moveCmd = distances[dist]
      
        if platform == 1 or platform == 3:
          if direction == 4:
            moveCmd += " forward"
          else:
           moveCmd += " " + directions[int(direction)]
        if platform == 2:
          moveCmd += " forward"
        
        if platform == 3:
          tbArmCmd.publish("wave")
        
        tbMovementCmd.publish(moveCmd)
        print "Commands sent"
        while tbMovementState != "running" and not rospy.is_shutdown():
          rospy.sleep(0.05)
        print "Moving to gesture point"
        tbMovementCmd.publish("")
        mode = 1

    elif mode == 1:
      while tbMovementState != "finished" and not rospy.is_shutdown():
        rospy.sleep(0.1)
      print "Gesture point reached"
      if platform == 1:
        if direction == 4:
          tbMovementCmd.publish("spin")
          print "Starting spin behavior"
          rospy.sleep(20)
        else:        
          tbMovementCmd.publish("oscillate")  
          print "Starting oscillation behavior"
          rospy.sleep(20)
        mode = 2
      else:
        if platform == 3:
          if direction == 4:
            tbArmCmd.publish("cross")
          else:
            tbArmCmd.publish("point")
        startTime = time.time()
        rospy.sleep(20)
        if platform == 3:
          tbArmCmd.publish("")
      mode = 2
    elif mode == 2:
      if dist == 3:
        mode = 0
      else:
        if dist == 1:
          tbMovementCmd.publish("return near")
        elif dist == 2:
          tbMovementCmd.publish("return far")
        print "Returning to start point"
        while tbMovementState != "running" and not rospy.is_shutdown():
          rospy.sleep(0.1)
        tbMovementCmd.publish("")
        while tbMovementState != "finished" and not rospy.is_shutdown():
          rospy.sleep(0.1)
        mode = 0
    rospy.sleep(0.1)
  

