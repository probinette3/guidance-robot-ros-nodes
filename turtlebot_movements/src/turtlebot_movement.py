#!/usr/bin/env python

# node to do the bulk of the work moving the turtlebot from various points for demo video
# turtlebot leaves starting position, turns towards demonstration point, proceeds towards demonstration point
# then informs master node that it is finished
# finally, it returns to the starting point

# master node can send this node a selection of commands telling it to move to the "far" or "near" demo point or demonstrate in place
# master node can also send it commands to have the robot oscillate in a certain direction (to indicate that evacuee should proceed in that direction)
# or to have the robot spin in place (to indicate that participants should stay in place)

import roslib
import rospy
import time
import math
import actionlib
import tf

from geometry_msgs.msg import *
from move_base_msgs.msg import *
from nav_msgs.msg import Odometry
from std_msgs.msg import String
from copy import copy

turnRate = 1.0
speed = 0.5

turnDist = 4.0
nearDist = 6.0
farDist = 25.0

te = 0.02

currentPose = Pose()
cmd = ""

def cmdCB(msg):
  global cmd
  cmd = msg.data
  print "Command: ", cmd

def odomCB(msg):
  global currentPose
  currentPose = msg.pose.pose

def turn(currentTheta, targetTheta, vel_pub):
  if targetTheta - currentTheta > 3.14:
    targetTheta -= 6.28
  elif targetTheta - currentTheta < -3.14:
    targetTheta += 6.28
  twist = Twist()
  dTheta = 10*(targetTheta-currentTheta)
  if dTheta >= 0:
    twist.angular.z = min(turnRate, dTheta)
  else:
    twist.angular.z = max(-turnRate, dTheta)
  vel_pub.publish(twist)
  

def distFormula(point1, point2):
  return math.sqrt(pow(point1.x - point2.x,2) + pow(point1.y - point2.y,2))

def moveForward(dist, startPoint, currentPoint, currentTheta, targetTheta, startTime, speed, vel_pub):
  """Drives forward dist"""
  rampTime = 1.0 # ideal ramp up/down time in seconds
  rampDist = (speed/2.0)*rampTime
    
  thisSpeed = 0
  
  if dist < 2*rampDist:
    rampDist = dist/2.0
    rampTime = rampDist/(speed/2.0)
  
  curDist = distFormula(startPoint, currentPoint)
  if curDist < rampDist:
    thisSpeed = max(0.1,(curDist/rampDist)*speed)
  elif curDist < dist - rampDist:
    thisSpeed = speed
  elif curDist < dist:
    thisSpeed = max(0.1,((dist - curDist)/rampDist)*speed)
  else:
    thisSpeed = 0
  
  if targetTheta - currentTheta > 3.14:
    targetTheta -= 6.28
  elif targetTheta - currentTheta < -3.14:
    targetTheta += 6.28
  
  twist = Twist()
  twist.linear.x = thisSpeed
  twist.angular.z = 10.0*(targetTheta - currentTheta)
  vel_pub.publish(twist)
  return 0  
    
if __name__ == "__main__":
  rospy.init_node("turtlebot_movement")
  rospy.Subscriber('odom',Odometry,odomCB)
  vel_pub = rospy.Publisher("/cmd_vel_mux/input/safety_controller", Twist)
  rospy.Subscriber("/turtlebot_movement_command", String, cmdCB)
  cmd_pub = rospy.Publisher("/turtlebot_movement_response", String)
  
  
  rospy.sleep(1)
  
  mode = 0
  moveMode = 0
  startTime = 0.0
  startPose = copy(currentPose)
  initTurn = 0.0
  firstDist = 0.0
  firstTurn = 0.0
  secondDist = 0.0
  secondTurn = 0.0
  oscDir = 1.0
  oscCenter = 0.0
  oscMode = 0  

  while not rospy.is_shutdown():
    pos = copy(currentPose.position)
    
    q = (currentPose.orientation.x,currentPose.orientation.y,
         currentPose.orientation.z,currentPose.orientation.w)
    
    (phi, psi, theta) = tf.transformations.euler_from_quaternion(q)
    
    if mode == 0:
      startTime = time.time()
      startPos = copy(currentPose.position)
      
      if "spin" in cmd:
        turn(theta, theta + 1, vel_pub)
      elif "oscillate" in cmd:
        if oscMode == 0:
          oscCenter = theta
          oscDir = 1.0
          oscMode = 1
        else:
          dTheta = oscCenter + oscDir*0.525
          if dTheta > 3.14:
            dTheta -= 6.28
          if dTheta < -3.14:
            dTheta += 6.28
          turn(theta, dTheta, vel_pub)
          print oscCenter, oscDir
          if abs(theta - dTheta) < te:
            oscDir *= -1.0
      else:
        oscMode = 0

      if "left" in cmd:
        secondTurn = 3.13
      elif "forward" in cmd:
        secondTurn = -1.57
      elif "backward" in cmd:
        secondTurn = 1.57
      
      if "return" in cmd:
        mode = -1
      elif "near" in cmd:
        firstDist = turnDist
        secondDist = nearDist
        firstTurn = -1.57
        mode = 1 
      elif "far" in cmd:
        firstDist = turnDist
        secondDist = farDist
        firstTurn = 1.57
        mode = 1
      elif "stay" in cmd:
        firstDist = 0
        secondDist = 0
        firstTurn = theta
        mode = 1
      initTurn = 0.0
        
    elif mode == -1:
      if "far" in cmd:
        initTurn = -1.57
        turn(theta, initTurn, vel_pub)
        if abs(theta - initTurn) < te:
          mode = -2
      elif "near" in cmd:
        initTurn = 1.57
        turn(theta, initTurn,vel_pub)
        if abs(theta - initTurn) < te:
          mode = -2
    elif mode == -2:
      if "far" in cmd:
        firstDist = farDist
        secondDist = turnDist
        firstTurn = 3.13
        secondTurn = 0
        mode = 1
      elif "near" in cmd:
        firstDist = nearDist
        secondDist = turnDist
        firstTurn = 3.13
        secondTurn = 0
        mode = 1
         
    elif mode == 1:
      cmd_pub.publish("running")
      moveForward(firstDist, startPos, pos, theta, initTurn, startTime, speed, vel_pub)
      if abs(distFormula(startPos, pos) - firstDist) < 0.05:    
        startTime = time.time()
        startPos = copy(currentPose.position)
        mode = 2
    elif mode == 2:
      turn(theta, firstTurn, vel_pub)
      if abs(theta - firstTurn) < te:
        startTime = time.time()
        startPos = copy(currentPose.position)
        mode = 3
    elif mode == 3:
      moveForward(secondDist, startPos, pos, theta, firstTurn, startTime, speed, vel_pub)
      if abs(distFormula(startPos, pos)- secondDist) < 0.05:
        startTime = time.time()
        startPos = copy(currentPose.position)
        mode = 4
    elif mode == 4:
      turn(theta, secondTurn, vel_pub)
      if abs(theta - secondTurn) < te:
        startTime = time.time()
        startPos = copy(currentPose.position)
        mode = 5
    elif mode == 5:
      cmd_pub.publish("finished")
      startTime = time.time()
      startPos = copy(currentPose.position)
      mode = 0
    rospy.sleep(0.01)
