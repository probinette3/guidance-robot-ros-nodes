#!/usr/bin/env python

# This node receives serial information from an Arduino
# The Arduino receives PWM from an RC controller and transmits those values over serial

import serial
import roslib
import rospy
import math

from geometry_msgs.msg import *
from std_msgs.msg import String

if __name__ == "__main__":
  rospy.init_node("arduino_control")
  # publish to safety controller, let the TurtleBot code handle the rest
  vel_pub = rospy.Publisher("/cmd_vel_mux/input/safety_controller", Twist)
  # publish the RC switch to whichever node needs it, currently controls arms
  switch_pub = rospy.Publisher("rc_switch", String)

  ser = serial.Serial("/dev/RCControl", 115200)

  # variables to hold the linear speed, angular speed, and current value of the three position switch
  throttle = 0.0
  steering = 0.0
  switchCode = "0"
  
  while not rospy.is_shutdown():
    line = ser.readline()
    # parse serial
    if "rc" in line:
      values = line.split(":")[1].split(",")

      # only consider channels 2 (3 pos switch), 4 (throttle), and 5 (steering)
      throttle = (float(values[3]) - 3000.0)/700.0 # 3000 to center value around 0, scaling factor determined empirically
      steering = (float(values[4]) - 3000.0)/100.0
      if math.fabs(throttle) < 0.05 or math.fabs(throttle) > 3.0: # dead band
        throttle = 0.
      if math.fabs(steering) < 0.2 or math.fabs(steering) > 10.0: # dead band
        steering = 0.0

      # switch code values determined empirically because it does not seem to be linear. Might be due to digital in on Arduino
      if float(values[2]) > 3500:
        switchCode = "2"
      elif float(values[2]) < 2500:
        switchCode = "1"
      else:
        switchCode = "0"
      
    twist = Twist()
    twist.linear.x = throttle
    twist.angular.z = steering
    vel_pub.publish(twist)
    switch_pub.publish(switchCode)
    
