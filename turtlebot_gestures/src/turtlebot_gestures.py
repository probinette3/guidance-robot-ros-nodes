#!/usr/bin/env python

# node to perform simple turtlebot motion gesture: oscillates 30 degrees in each direction to indicate evacuee should proceed in that direction

import roslib
import rospy
import time
import math

from geometry_msgs.msg import Twist
from std_msgs.msg import String

turnRate = 1.0
oscillateToAngle = 1.5 # Not really an angle, but manages to turn the turtlebot 30 degrees
cmd = ""


def cmdCB(msg):
  global cmd
  cmd = msg.data
  print "Command: ", cmd

def oscillate(mode, startTime):
    global turnRate
    global oscillateToAngle

    if mode == 0:
      startTime = time.time()
      mode = 1
    elif mode == 1:
      if time.time() - startTime > oscillateToAngle/turnRate:
        turnRate *= -1
        mode = 2
        startTime = time.time()
    elif mode == 2:
        if time.time() - startTime > 2.0*oscillateToAngle/math.fabs(turnRate):
          startTime = time.time()
          turnRate *= -1
      
    twist.angular.z = turnRate
    vel_pub.publish(twist)

    return mode,startTime



if __name__ == "__main__":
  rospy.init_node("turtlebot_gestures")
  vel_pub = rospy.Publisher("/cmd_vel_mux/input/safety_controller", Twist)
  cmd_pub = rospy.Publisher("/turtlebot_gesture_response", String)
  rospy.Subscriber("/turtlebot_gesture_command", String, cmdCB)
  twist = Twist()

  mode = 0
  startTime = 0
  superMode = 0
  count = 0 
  while not rospy.is_shutdown():
    if superMode == 0:
      if cmd == "start":
        cmd_pub.publish("running")
        superMode = 1
    elif superMode == 1:
      prevStart = startTime
      mode,startTime = oscillate(mode,startTime)
      if prevStart != startTime:
        count += 1
        print count
      if count > 20:
        count = 0
        superMode = 0
        cmd_pub.publish("finished")
    rospy.sleep(0.01)
