#!/usr/bin/env python

# This node controls two Pincher AX-12 arms to perform the following gestures:
# Wave - wave from side to side to attract attention
# Point - point in direction evacuee should proceed
# Cross - cross arms to inform evacuee to stop

import rospy
from std_msgs.msg import String
from arbotix_python.arbotix import ArbotiX
from arbotix_python.servo_controller import *
import time

port0 = "/dev/leftArm"
port1 = "/dev/rightArm"

# values for arm positions
center = 512

waveMin = 512
waveMax = 712
waveDirection0 = 800 # left arm
waveDirection1 = 200 # right arm

pointMin = 650
pointMax = 850
pointDirection = 512

crossMin = 512
crossMax = 642
crossDirection0 = 350 # left arm
crossDirection1 = 900 # right arm

servoSpeed = 75
servoError = 10

class armMotions:
    def __init__(self, pubName, controller, mirror=False):
        self.gesture = "None"
        self.waveMode = True 
        self.ready = True
        self.otherReady = True
        self.con = controller
        self.mirror = mirror
        self.readyPub = rospy.Publisher(pubName, String)

    def centerArm(self):
      """Point arm straight up"""
        for i in range(5,0,-1):
            self.con.setPosition(i,center)
            rospy.sleep(1)
        centered = True
        for i in range(1,6):
            if abs(self.con.getPosition(i) - center) > servoError:
                centered = False
        return centered

    def waveArm(self,mode):
        """Wave arm from side to side
        mode controls whether we are moving left or right"""
        if self.otherReady == "wait": # wait for other arm to be ready
            return mode
        goal = waveMin
        if mode:
            goal = waveMax
        self.con.setPosition(2,goal)
        if self.mirror:
          self.con.setPosition(1,waveDirection1)
        else:
          self.con.setPosition(1,waveDirection0)
        return self.checkPosition(goal, mode)

    def pointArm(self,mode):
        """Point arm forward and wave it a bit
        mode controls whether we are moving up or down"""
        if self.otherReady == "wait": # wait for other arm to be ready
            return mode
        goal = pointMin
        if mode:
            goal = pointMax
        self.con.setPosition(1,pointDirection)
        self.con.setPosition(2,goal)
        return self.checkPosition(goal, mode)

    def crossArm(self,mode):
        """Cross arm through center so other arm will cross it
        mode controls whether we are moving left or right"""
        if self.otherReady == "wait": # wait for other arm to be ready
            return mode
        goal = crossMin
        if mode:
            goal = crossMax
        if self.mirror:
          self.con.setPosition(1,crossDirection1)
        else:
          self.con.setPosition(1,crossDirection0)
        self.con.setPosition(2,goal)
        return self.checkPosition(goal, mode)
        
    def checkPosition(self, goal, mode):
        """Check to see if we are within error tolerances of our goal and switch modes if we are AND the other arm is"""
        if abs(self.con.getPosition(2) - goal) < servoError:
            self.ready = "ready"
        else:
            self.ready = "moving"
        self.readyPub.publish(self.ready)
        if self.ready == "ready" and self.otherReady == "ready":
            rospy.sleep(0.5)
            return not mode
        else:
            return mode

    def gestureCallBack(self,data):
        self.gesture = data.data
    
    def readyCallBack(self, data):
        self.otherReady = data.data
    
    def rcControlCallBack(self, data):
        """grab value of the three position switch on the controller"""
        if data.data == "0":
          self.gesture = "center"
        elif data.data == "1":
          self.gesture = "wave"
        elif data.data == "2":
          self.gesture = "point"
              
    def doGesture(self,event):
        """function to control the gesture requested by the controller or other node"""
        if self.gesture == "wave":
            self.waveMode = self.waveArm(self.waveMode)
        elif self.gesture == "point":
            self.waveMode = self.pointArm(self.waveMode)
        elif self.gesture == "cross":
            if abs(controller.getPosition(1) - crossDirection) > servoError:
              self.con.setPosition(1,crossDirection)
              self.con.setPosition(2,center)
              self.ready = "wait"
              self.readyPub.publish(self.ready)
            elif self.ready == "wait":
              self.ready = "ready"
              self.readyPub.publish(self.ready)
              rospy.sleep(0.5)
            else:
              self.waveMode = self.crossArm(self.waveMode)
        else: # if we get nonsense, center the arm
            self.centerArm()
        time.sleep(0.1)
        
if __name__ == "__main__":

        # set up arbotix controllers
        controller0 = ArbotiX(port=port0)
        controller1 = ArbotiX(port=port1)
        
        arm0 = armMotions("/arm0Ready", controller0)
        arm1 = armMotions("/arm1Ready", controller1, True)

        # start by centering both arms
        while arm0.centerArm()==False or arm1.centerArm()==False:
            time.sleep(0.1)

        # set the speed for every servo on both arms
        for i in range(1,6):
            controller0.setSpeed(i,servoSpeed)
            controller1.setSpeed(i,servoSpeed)
            time.sleep(0.1)
                
        print "Ready"
        
        rospy.init_node("arm_motion")
        rospy.Timer(rospy.Duration(0.1), arm0.doGesture)
        rospy.Subscriber("/arm_commands", String, arm0.gestureCallBack)
        rospy.Subscriber("/arm1Ready", String, arm0.readyCallBack)
        rospy.Subscriber("/rc_switch", String, arm0.rcControlCallBack)

        rospy.Timer(rospy.Duration(0.1), arm1.doGesture)
        rospy.Subscriber("/arm_commands", String, arm1.gestureCallBack)
        rospy.Subscriber("/arm0Ready", String, arm1.readyCallBack)
        rospy.Subscriber("/rc_switch", String, arm1.rcControlCallBack)
        
        
        rospy.spin()

        # relax all servos
        print "Exiting..."
        for i in range(1,6):
            time.sleep(0.1)
            controller0.disableTorque(i)
            controller1.disableTorque(i)
        
        
    
