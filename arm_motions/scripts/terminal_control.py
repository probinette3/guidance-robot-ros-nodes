#!/usr/bin/env python

# simple node for controlling the arms from the terminal

import rospy
from std_msgs.msg import String

gestures = ["None", "wave", "point", "cross"]
    

def talker():
    pub = rospy.Publisher("arm_commands", String)
    rospy.init_node("arm_terminal_control")
    r = rospy.Rate(10)
    while not rospy.is_shutdown():
        cmd = input("Select arm command\n1: Wave\n2: Point\n3: Cross\n>> ")
        pub.publish(gestures[cmd])
        r.sleep()

if __name__ == "__main__":
    talker()
